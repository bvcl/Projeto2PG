#include <stdio.h>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include "OperacoesBase.cpp"

std::vector<Ponto>  varredura(Triangulo T){
      double constIteration = 1;
      std::vector<Ponto> v;
      double yScan,yini,yfim,xini,xfim;
      xini=T.getP1().getX();
      if(T.getP2().getX()<xini)xini=T.getP2().getX();
      if(T.getP3().getX()<xini)xini=T.getP3().getX();

      xfim=T.getP1().getX();
      if(T.getP2().getX()>xfim)xfim=T.getP2().getX();
      if(T.getP3().getX()>xfim)xfim=T.getP3().getX();

      yini=T.getP1().getY();
      if(T.getP2().getY()>yini)yini=T.getP2().getY();
      if(T.getP3().getY()>yini)yini=T.getP3().getY();

      yfim=T.getP1().getX();
      if(T.getP2().getX()<yfim)yfim=T.getP2().getX();
      if(T.getP3().getX()<yfim)yfim=T.getP3().getX();

      yScan=yini;
      SegmentoReta P1P2 = SegmentoReta(T.getP1(),T.getP2());
      P1P2.makeLine();
      SegmentoReta P1P3 = SegmentoReta(T.getP1(),T.getP3());
      P1P3.makeLine();
      SegmentoReta P2P3 = SegmentoReta(T.getP2(),T.getP3());
      P2P3.makeLine();

      for(double i=xini;i<=xfim;i+=constIteration){
        if(abs(yScan-yini)<0.00000001 || abs(yScan-yfim)<0.00000001){
          bool found = false;
          while(P1P2.isInLine(i,yScan) || P1P3.isInLine(i,yScan) || P2P3.isInLine(i,yScan)){
            found=true;
            v.push_back(Ponto(i,yScan));
            printf("added %f %f\n",i,yScan);
            i+=constIteration;
            if(i>xfim)break;
          }
          if(found){
            i=xini-constIteration;
            yScan-=constIteration;
          }
        }
        else if(P1P2.isInLine(i,yScan)){
          while(!P1P3.isInLine(i,yScan) && !P2P3.isInLine(i,yScan)){
            v.push_back(Ponto(i,yScan));
            printf("added %f %f\n",i,yScan);
            i+=constIteration;
          }
          v.push_back(Ponto(i,yScan));
          printf("added %f %f\n",i,yScan);
          i=xini;
          yScan-=constIteration;
        }
        else if(P1P3.isInLine(i,yScan)){
          while(!P1P2.isInLine(i,yScan) && !P2P3.isInLine(i,yScan)){
            v.push_back(Ponto(i,yScan));
            printf("added %f %f\n",i,yScan);
            i+=constIteration;
          }
          v.push_back(Ponto(i,yScan));
          printf("added %f %f\n",i,yScan);
          i=xini;
          yScan-=constIteration;
        }
        else if(P2P3.isInLine(i,yScan)){
          while(!P1P3.isInLine(i,yScan) && !P1P2.isInLine(i,yScan)){
            v.push_back(Ponto(i,yScan));
            printf("added %f %f\n",i,yScan);
            i+=constIteration;
          }
          v.push_back(Ponto(i,yScan));
          printf("added %f %f\n",i,yScan);
          i=xini;
          yScan-=constIteration;
        }
        if(yScan<yfim)break;
      }
      return v;
}

std::vector<Ponto> fillBottomFlatTriangle(Triangulo T){
 double constIteration=0.1;
 std::vector<Ponto> v;
 Ponto v1 = T.getP1();
 Ponto v2 = T.getP2();
 Ponto v3 = T.getP3();
 float invslope1 = (v2.getX() - v1.getX()) / (v2.getY() - v1.getY());
 float invslope2 = (v3.getX() - v1.getX()) / (v3.getY() - v1.getY());

 float curx1 = v1.getX();
 float curx2 = v1.getX();
printf("antes do for1\n" );
 for (double scanlineY = v1.getY(); scanlineY >= v2.getY()+0.000001; scanlineY-=constIteration){
   printf("inside for1\n" );
   for(curx1;curx1<=curx2+0.00001;curx1+=constIteration){
     printf("pushing\n" );
     v.push_back(Ponto(curx1,scanlineY));
   }
   curx1 += invslope1;
   curx2 += invslope2;
 }
 return v;
}

std::vector<Ponto> fillTopFlatTriangle(Triangulo T){
 double constIteration=0.1;
 std::vector<Ponto> v;
 Ponto v1 = T.getP1();
 Ponto v2 = T.getP2();
 Ponto v3 = T.getP3();
 float invslope1 = (v3.getX() - v1.getY()) / (v3.getY() - v1.getY());
 float invslope2 = (v3.getX() - v2.getX()) / (v3.getY() - v2.getY());

 float curx1 = v3.getX();
 float curx2 = v3.getX();
printf("antes do for1\n" );
 for (double scanlineY = v3.getY(); scanlineY < v1.getY()+0.0000001; scanlineY+=constIteration){
   printf("inside for2\n" );
   for(curx1;curx1<=curx2+0.00001;curx1+=constIteration){
    printf("pushing\n");
     v.push_back(Ponto(curx1,scanlineY));
   }
   curx1 -= invslope1;
   curx2 -= invslope2;
 }
 return v;
}

Triangulo sortVerticesAscendingByY(Triangulo T){
  Ponto aux,aux2,aux3;
  bool P1a=true,P1b=false,P1c=false;
  bool P3a=true,P3b=false,P3c=false;
  aux=T.getP1();
  if(T.getP2().getY()>aux.getY()){
    P1a=false;
    P1b=true;
    aux=T.getP2();
  }
  if(T.getP3().getY()>aux.getY()){
    P1a=false;
    P1b=false;
    P1c=true;
    aux=T.getP3();
  }

  aux3=T.getP1();
  if(T.getP2().getY()<aux3.getY()){
    aux3=T.getP2();
    P3a=false;
    P3b=true;
  }
  if(T.getP3().getY()<aux3.getY()){
    P3a=false;
    P3b=false;
    P3c=true;
    aux3=T.getP3();
  }
  //aux=P1 P1=maiorY
  if(P1a){
    if(P3b){
      aux2=T.getP3();
    }
    else if(P3c){
      aux2=T.getP2();
    }
  }
  //aux=P2 P2=maiorY
  else if(P1b){
    if(P3a){
      aux2=T.getP3();
    }
    else if(P3c){
      aux2=T.getP1();
    }
  }
  //aux=P3 P3=maiorY
  else if(P1c){
    if(P3a){
      aux2=T.getP2();
    }
    else if(P3b){
      aux2=T.getP1();
    }
  }

  Triangulo Tresp;
  Tresp.setP1(aux);
  Tresp.setP2(aux2);
  Tresp.setP3(aux3);
  return Tresp;
}

std::vector<Ponto> drawTriangle(Triangulo T){
   // at first sort the three vertices by y-coordinate ascending so v1 is the topmost vertice
  T = sortVerticesAscendingByY(T);
  std::vector<Ponto> v;
  // here we know that v1.y <= v2.y <= v3.y
  // check for trivial case of bottom-flat triangle
  if (abs(T.getP2().getY()-T.getP3().getY())<0.000001){
    printf("aqui\n" );
    v=fillBottomFlatTriangle(T);
  }
  // check for trivial case of top-flat triangle
  else if (abs(T.getP1().getY()-T.getP2().getY())<0.000001){
    printf("aqui2\n" );
    v=fillTopFlatTriangle(T);
  }
  else
  {

    // general case - split the triangle in a topflat and bottom-flat one
    Ponto v4 = Ponto(
      (T.getP1().getX() + ((float)(T.getP2().getY() - T.getP1().getY()) / (float)(T.getP3().getY() - T.getP1().getY())) * (T.getP3().getX() - T.getP1().getX())), T.getP2().getY());
      Triangulo t1 = T;
      t1.setP3(v4);
      Triangulo t2 = T;
      t2.setP1(t2.getP2());
      t2.setP2(v4);
      std::vector<Ponto> v2;

    v=fillBottomFlatTriangle(t1);
    v2=fillTopFlatTriangle(t2);

    for(int i=0;i<v2.size();i++){
      v.push_back(v2[i]);
    }
  }
  return v;
}

std::vector<Ponto> toReturn(){
  Ponto P1 = Ponto (1,3);
  Ponto P2 = Ponto (3,6);
  Ponto P3 = Ponto (5,1);

  std::vector<Ponto> v;
  Triangulo T = Triangulo(P1,P2,P3);
  v=varredura(T);
  return v;
}

int main(){
  Ponto P1 = Ponto (1,3);
  Ponto P2 = Ponto (3,6);
  Ponto P3 = Ponto (5,1);

  Ponto P4 = Ponto (1,7);
  Ponto P5 = Ponto (3,2);
  Ponto P6 = Ponto (5,5);

  Triangulo T = Triangulo(P4,P5,P6);
  T = sortVerticesAscendingByY(T);
  T.printPoints2D();
  std::vector<Ponto> v;
  v=drawTriangle(T);

  //v=varredura(T);
  printf("%lu\n",v.size());
}
