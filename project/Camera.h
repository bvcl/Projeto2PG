#include "ClassesBase"
class Camera{
  private:
    double d;
    double hx;
    double hy;
    Vetor v;
    Vetor n;
    Ponto posicao;
  public:
    getD(){return this->d};
    getHx(){return this->hx};
    getHy(){return this->hy};
    getV(){return this->v};
    getN(){return this->n};
    getPosicoa(){return this->posicao};
    setD(double d){this->d = d};
    setHx(double hx){this->hx = hx};
    setHy(double hy){this->hy = hy};
    setV(Vetor v){this->v = v};
    setN(Vetor n){this->n = n};
    setPosicao(Ponto posicao){this->posicao = posicao};

    Camera(int d, int hx, int hy, Vetor v, Vetor n, Ponto posicao){
      this->d  = d;
      this->hx = hx;
      this->hy = hy;
      this->v = v;
      this->n = n;
      this->posicao = posicao;
    }
}
