#include <stdio.h>
#include <math.h>
class Vetor {
  private:
    double x;
    double y;
    double z;
  public:
    double getX(){return this->x;}
    double getY(){return this->y;}
    double getZ(){return this->z;}
    double setX(double x){this->x=x;}
    double setY(double y){this->y=y;}
    double setZ(double z){this->z=z;}

    Vetor(int x, int y, int z){this->x=x;this->y=y;this->z=z;}
    Vetor(){this->x=0;this->y=0;this->z=0;};
    void normalizar(){
      double normaV = sqrt(((this->getX())*(this->getX())) + ((this->getY())*(this->getY())) + ((this->getZ())*(this->getZ())));
      setX((this->x)/normaV);
      setY((this->y)/normaV);
      setZ((this->z)/normaV);
    }
    void corrigir(Vetor u){
      //vai usar a projecao para corrigir o vetor
      double vn = ((this->getX()*u.getX())+(this->getY()*u.getY()) + (this->getZ()*u.getZ()));
      double n2 = ((u.getX()*u.getX())+(u.getY()*u.getY()) + (u.getZ()*u.getZ()));
      double k = vn/n2;
      setX(this->getX()-(k*(u.getX())));
      setY(this->getY()-(k*(u.getY())));
      setZ(this->getZ()-(k*(u.getZ())));
    }
    void printVetor(){
      printf("%f %f %f\n",this->getX(),this->getY(),this->getZ());
    }
};

class Ponto{
  private:
    double x;
    double y;
    double z;
  public:
    double getX(){return this->x;}
    double getY(){return this->y;}
    double getZ(){return this->z;}
    double setX(double x){this->x=x;}
    double setY(double y){this->y=y;}
    double setZ(double z){this->z=z;}
    Ponto(int x, int y, int z){this->x=x;this->y=y;this->z=z;}
    Ponto(int x, int y){this->x=x;this->y=y;}
    Ponto(){this->x=0;this->y=0;this->z=0;};
    void printPonto3D(){
      printf("%f %f %f\n",this->getX(),this->getY(),this->getZ());
    }
    void printPonto2D(){
      printf("%f %f\n",this->getX(),this->getY());
    }
};

class Triangulo{
  private:
    Ponto P1;
    Ponto P2;
    Ponto P3;
  public:
    Triangulo(Ponto P1, Ponto P2, Ponto P3){
      this->P1=P1; this->P2=P2;this->P3=P3;
    }
    Triangulo(){};
    Ponto getP1(){return this->P1;}
    Ponto getP2(){return this->P2;}
    Ponto getP3(){return this->P3;}
    void setP1(Ponto P){this->P1=P;}
    void setP2(Ponto P){this->P2=P;}
    void setP3(Ponto P){this->P3=P;}
    void printPoints3D(){
      printf("%f %f %f\n", this->getP1().getX(),this->getP1().getY(),this->getP1().getZ());
      printf("%f %f %f\n", this->getP2().getX(),this->getP2().getY(),this->getP2().getZ());
      printf("%f %f %f\n", this->getP3().getX(),this->getP3().getY(),this->getP3().getZ());
    }
    void printPoints2D(){
      printf("%f %f \n", this->getP1().getX(),this->getP1().getY());
      printf("%f %f \n", this->getP2().getX(),this->getP2().getY());
      printf("%f %f \n", this->getP3().getX(),this->getP3().getY());
    }
};

class SegmentoReta{
  private:
    Ponto p1;
    Ponto p2;
    std::vector<Ponto> v;
    double a;
    double b;
  public:
    SegmentoReta(Ponto p1, Ponto p2){this->p1=p1;this->p2=p2;}
    double getA(){return this->a;}
    double getB(){return this->b;}
    void makeLine(){
      this->a = (p1.getY()-p2.getY())/(p1.getX()-p2.getX());
      this->b = (p1.getY()-(a*p1.getX()));
    }
    bool containsPoint(double x, double y){
      double crossproduct = (y - p1.getY()) * (p2.getX() - p1.getX()) - (x - p1.getX()) * (p2.getY() - p1.getY());
      if (abs(crossproduct) > 0.00001)return false;

      double dotproduct = (x - p1.getX()) * (p2.getX() - p1.getX()) + (y - p1.getY())*(p2.getY() - p1.getY());
      if (dotproduct < 0) return false;

      double squaredlengthba = (p2.getX() - p1.getX())*(p2.getX() - p1.getX()) + (p2.getY() - p1.getY())*(p2.getY() - p1.getY());
      if (dotproduct > squaredlengthba) return false;

      return true;
    }
    bool isInLine(double x, double y){
      if( abs((((this->a)*x)+b)-y) < 0.00000001)return true;
      else return false;
    }
};
