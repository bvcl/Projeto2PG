#include "ClassesBase.h"
class Iluminacao{
  private:
    Ponto l;
    double ka;
    Vetor ia;
    double kd;
    Vetor od;
    double ks;
    Cor il;
    int mi;
  public:
    getL(){return this->l};
    getKa(){return this->ka};
    getIa(){return this->ia};
    getKd(){return this->kd};
    getOd(){return this->od};
    getKs(){return this->ks};
    getIl(){return this->il};
    getMi(){return this->mi};

    setL(Ponto l){this->l = l};
    setKa(double ka){this->ka = ka};
    setIa(Vetor ia){this->ia = ia};
    setKd(double kd){this->kd = kd};
    setOd(Vetor kd){this->od = od};
    setKs(double ks){this->ks = ks};
    setIl(Cor il){this->il = il};
    setMi(int mi){this->mi = mi};
}

class Cor{
  private:
    double r;
    double g;
    double b;
  public:
    getR(){return this->r};
    getG(){return this->g};
    getB(){return this->b};
    setR(double r){this->r = r};
    setG(double g){this->g = g};
    setB(double b){this->b = b};

    Cor(double r, double g, double b){
      this->r = r;
      this->g = g;
      this->b = b;
    }
}
