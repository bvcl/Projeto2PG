#include <stdio.h>
#include <math.h>
#include "ClassesBase.h"
Vetor prodVetorial(Vetor n, Vetor v){
  Vetor U;
  U.setX((n.getY()*v.getZ())-(n.getZ()*v.getY()));
  U.setY((n.getZ()*v.getX())-(n.getX()*v.getZ()));
  U.setZ((n.getX()*v.getY())-(n.getY()*v.getX()));
  return U;
}

void multiplicar(double a[3][3], double b[3][1], double ** resp){
  double sum =0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 1; j++) {
      for (int k = 0; k < 3; k++) {
        sum = sum + a[i][k]*b[k][j];
      }
      resp[i][j]=sum;
      sum = 0;
    }
  }
}

//funcao para obter a matriz de mudanca de base da base canonica (coordenadas de mundo)
//para a base alfa (coordenadas de vista):
void matrizMudancaBase(Vetor u,Vetor v,Vetor n, double ** matrix){
  for(int i=0;i<3;i++){
    if(i==0){
      matrix[i][0]=u.getX();
      matrix[i][1]=u.getY();
      matrix[i][2]=u.getZ();
    }
    else if(i==1){
      matrix[i][0]=v.getX();
      matrix[i][1]=v.getY();
      matrix[i][2]=v.getZ();
    }
    else{
      matrix[i][0]=n.getX();
      matrix[i][1]=n.getY();
      matrix[i][2]=n.getZ();
    }
  }
}
//preencher uma matrix com a subtração de pontos
void fillMatrix(double m[3][1],Ponto P, Ponto C){
  m[0][0]=P.getX()-C.getX();
  m[1][0]=P.getY()-C.getY();
  m[2][0]=P.getZ()-C.getZ();
}
//converter uma matrix que representa um ponto em um ponto
Ponto matrixToPoint(double m[3][1]){
  Ponto resp;
  resp.setX(m[0][0]);
  resp.setY(m[1][0]);
  resp.setZ(m[2][0]);
  return resp;
}

Ponto obterPLinha(Ponto pontoAux,double d,double hx,double hy){
  Ponto resp;
  resp.setX((pontoAux.getX()/pontoAux.getZ()) * (d/hx));
  resp.setY((pontoAux.getY()/pontoAux.getZ()) * (d/hy));
  return resp;
}

Ponto obterPTela(Ponto p,double resX,double resY){
  Ponto resp;
  resp.setX(((p.getX()+1)/2)*resX);
  resp.setY(((1-p.getY())/2)*resY);
  return resp;
}

Triangulo obterTriangCoordTela(Triangulo T, Ponto C, Vetor V, Vetor N, double resolucaoX, double resolucaoY, double d,double hx, double hy){
  //corrige e normaliza os vetores necessario. Calcula o vetor U
  V.corrigir(N);
  V.normalizar();
  N.normalizar();
  Vetor U = prodVetorial(N,V);
  //faz a matriz mudanca de base
  double matrix [3][3];
  double *auxM[3]={matrix[0],matrix[1],matrix[2]};
  matrizMudancaBase(U,V,N,auxM);
  //faz as matrizes de P-C
  double MP1C [3][1];
  double MP2C [3][1];
  double MP3C [3][1];
  fillMatrix(MP1C,T.getP1(),C);
  fillMatrix(MP2C,T.getP2(),C);
  fillMatrix(MP3C,T.getP3(),C);
  //matriz resultante da multiplciacao de I * [P-C]
  double MP1V[3][1];
  double MP2V[3][1];
  double MP3V[3][1];
  double *auxMatrix[3] = { MP1V[0], MP1V[1], MP1V[2]};
  multiplicar(matrix,MP1C,auxMatrix);
  double *auxMatrix2[3]={MP2V[0], MP2V[1], MP2V[2]};
  multiplicar(matrix,MP2C,auxMatrix2);
  double *auxMatrix3[3]={ MP3V[0], MP3V[1], MP3V[2]};
  multiplicar(matrix,MP3C,auxMatrix3);
  //converte as matrizes acima para pontos
  Ponto pontoV1=matrixToPoint(MP1V);
  Ponto pontoV2=matrixToPoint(MP2V);
  Ponto pontoV3=matrixToPoint(MP3V);
  //calcula P' para cada ponto (projeção em perspectiva)
  Ponto P1Linha = obterPLinha(pontoV1,d,hx,hy);
  Ponto P2Linha = obterPLinha(pontoV2,d,hx,hy);
  Ponto P3Linha = obterPLinha(pontoV3,d,hx,hy);
  //calcula P em coordenada de tela
  Ponto P1Tela = obterPTela(P1Linha,resolucaoX,resolucaoY);
  Ponto P2Tela = obterPTela(P2Linha,resolucaoX,resolucaoY);
  Ponto P3Tela = obterPTela(P3Linha,resolucaoX,resolucaoY);
  //Triangulo em coordenadas de tela resultante
  Triangulo TResp = Triangulo(P1Tela,P2Tela,P3Tela);
  return TResp;
}

/*int main(){
  Ponto C = Ponto(1,2,-3);
  Vetor V = Vetor(0,0,1);
  Vetor N = Vetor(-1,-2,2);
  Ponto P1 = Ponto (1,0,0);
  Ponto P2 = Ponto (0,1,0);
  Ponto P3 = Ponto (0,0,1);
  double resolucaoX=1000;
  double resolucaoY=1000;
  double d = 1;
  double hx=2;
  double hy=2;
  Triangulo T = Triangulo(P1,P2,P3);
  T.printPoints();
  T = obterTriangCoordTela(T,C,V,N,resolucaoX,resolucaoY,d,hx,hy);
  T.printPoints();
}*/
